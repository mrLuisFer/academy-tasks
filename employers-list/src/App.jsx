import { useEffect, useState } from 'react'
import { ChakraProvider, Container, Flex, Spinner } from '@chakra-ui/react'
import Header from './components/Header'
import UserDetail from './components/UserDetail'
import { getRandomNumbers } from './lib/getRandomNumber'
import { getRandomProfileImg } from './lib/getRandomProfileImg'

const defaultUsers = [
  {
    email: 'luis@email.com',
    id: getRandomNumbers(),
    img: getRandomProfileImg(),
    name: 'Luis'
  },
  {
    email: 'fer@email.com',
    id: getRandomNumbers(),
    img: getRandomProfileImg(),
    name: 'Fernando'
  },
  {
    email: 'jn@email.com',
    id: getRandomNumbers(),
    img: getRandomProfileImg(),
    name: 'John'
  },
  {
    email: 'lol@email.com',
    id: getRandomNumbers(),
    img: getRandomProfileImg(),
    name: 'Lol'
  }
]

export default function App() {
  const [users, setUsers] = useState(defaultUsers)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const timeout = setTimeout(() => setLoading(false), 1000)

    return () => {
      clearTimeout(timeout)
    }
  }, [])

  return (
    <ChakraProvider>
      <Container paddingTop="1rem">
        <Header users={users} setUsers={setUsers} />
        {loading ? (
          <Flex justifyContent="center" mt="2rem">
            <Spinner size="xl" />
          </Flex>
        ) : (
          <>
            {users.map((user) => (
              <UserDetail key={user.id} user={user} users={users} setUsers={setUsers} />
            ))}
          </>
        )}
      </Container>
    </ChakraProvider>
  )
}
