import { Box, Button, ButtonSpinner, Container, Flex, FormControl, FormLabel, Heading, Input } from '@chakra-ui/react'
import { GrFormClose } from 'react-icons/gr'
import { createPortal } from 'react-dom'
import { getRandomNumbers } from '../../lib/getRandomNumber'
import { useState } from 'react'

export default function AddUserModal({ setModalVisible, users, setUsers }) {
  const [btnLoading, setBtnLoading] = useState(false)

  const htmlElement = document.getElementById('add-user')

  const handleAddUser = (e) => {
    e.preventDefault()
    setBtnLoading(true)

    const inputImg = e.target[0].value.trim()
    const inputName = e.target[1].value.trim()
    const inputEmail = e.target[2].value.trim()

    const newUserSchema = {
      email: inputEmail,
      id: getRandomNumbers(),
      img: inputImg,
      name: inputName
    }

    // TODO: Add regex validation for email and validate the length of the name

    setUsers((currentUsers) => [...currentUsers, newUserSchema])

    // Fake loading
    setTimeout(() => setModalVisible(false), 2000)
  }

  return (
    <>
      {createPortal(
        <Container
          alignItems="center"
          bg="rgba(10, 10, 10, 0.2)"
          display="flex"
          justifyContent="center"
          minH="100vh"
          minW="100vw"
          position="absolute"
          zIndex="10"
        >
          <Box bg="white" padding="1.5rem 1rem" borderRadius="md" w="450px">
            <Flex as="header" justifyContent="space-between" alignItems="center" mb="1rem">
              <Heading as="h2" size="1.2rem">
                Add user
              </Heading>
              <Button onClick={() => setModalVisible(false)}>
                <GrFormClose />
              </Button>
            </Flex>
            <Box as="form" display="flex" flexDirection="column" gap="0.9rem" onSubmit={(e) => handleAddUser(e)}>
              <FormControl>
                <FormLabel htmlFor="img">Add Image</FormLabel>
                <Input id="img" autoComplete="off" />
              </FormControl>
              <FormControl isRequired>
                <FormLabel htmlFor="name">Add Name</FormLabel>
                <Input id="name" type="name" autoComplete="off" />
              </FormControl>
              <FormControl isRequired>
                <FormLabel htmlFor="email">Add Email</FormLabel>
                <Input id="email" type="email" autoComplete="off" />
              </FormControl>
              <Button type="submit" colorScheme="teal">
                {btnLoading ? <ButtonSpinner /> : 'Submit'}
              </Button>
            </Box>
          </Box>
        </Container>,
        htmlElement
      )}
    </>
  )
}
