import { Button, useColorMode } from '@chakra-ui/react'
import { BsFillMoonStarsFill } from 'react-icons/bs'
import { RiSunFill } from 'react-icons/ri'

export default function DarkModeToggle() {
  const { colorMode, toggleColorMode } = useColorMode()

  return <Button onClick={toggleColorMode}>{colorMode === 'light' ? <BsFillMoonStarsFill /> : <RiSunFill />}</Button>
}
