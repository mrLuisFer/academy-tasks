import { Flex, Button, Heading, Fade } from '@chakra-ui/react'
import { useState } from 'react'
import AddUserModal from './AddUserModal'
import DarkModeToggle from './DarkModeToggle'

export default function Header({ users, setUsers }) {
  const [modalVisible, setModalVisible] = useState()

  return (
    <>
      <Flex as="header" justifyContent="space-between" w="lg">
        <Heading as="h1" size="lg">
          User Manager ({users.length})
        </Heading>
        <Flex gap="1rem">
          <Button onClick={() => setModalVisible(true)} type="button" colorScheme="messenger">
            Add
          </Button>
          <DarkModeToggle />
        </Flex>
      </Flex>
      <Fade in={modalVisible}>
        {modalVisible && <AddUserModal setModalVisible={setModalVisible} users={users} setUsers={setUsers} />}
      </Fade>
    </>
  )
}
