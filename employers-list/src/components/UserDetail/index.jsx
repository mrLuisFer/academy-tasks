import { useState } from 'react'
import { Text, VStack, Flex, Image, Box } from '@chakra-ui/react'
import UserActions from './UserActions'

export default function UserDetail({ user, setUsers, users }) {
  const [activeUser, setActiveUser] = useState(false)
  const [showIcons, setShowIcons] = useState(false)

  const imgSize = '60px'

  return (
    <VStack as="section">
      <Flex
        _hover={{
          boxShadow: '2px 2px 4px rgba(0,0,0,0.3)'
        }}
        _active={{
          boxShadow: '2px 2px 4px rgba(0,0,0,0.5)'
        }}
        alignItems="center"
        borderRadius="md"
        boxShadow="2px 2px 6px rgba(0,0,0,0.1)"
        justifyContent="space-between"
        margin="1rem 0"
        onMouseEnter={() => setShowIcons(true)}
        onMouseLeave={() => setShowIcons(false)}
        padding="1rem"
        transition="0.2s ease"
        w="xl"
      >
        <Flex gap="0.8rem" alignItems="center">
          <Box className={activeUser ? 'activeUser' : 'offlineUser'} position="relative" title={user.email}>
            <Image
              alt={`${user.name} profile photo`}
              border={`3px solid ${activeUser ? '#9AE6B4' : 'transparent'}`}
              borderRadius="50%"
              h={imgSize}
              objectFit="cover"
              src={user.img}
              transition="all 0.2s ease"
              w={imgSize}
            />
          </Box>
          <Text textTransform="capitalize">{user.name}</Text>
        </Flex>
        <UserActions
          activeUser={activeUser}
          setActiveUser={setActiveUser}
          setUsers={setUsers}
          showIcons={showIcons}
          users={users}
          user={user}
        />
      </Flex>
    </VStack>
  )
}
