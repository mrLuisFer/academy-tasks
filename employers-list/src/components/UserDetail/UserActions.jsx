import { Button, Fade, Flex } from '@chakra-ui/react'
import { BsCheck2All } from 'react-icons/all'
import { GrFormClose } from 'react-icons/gr'
import { BiTrashAlt } from 'react-icons/bi'

export default function UserActions({ user, setUsers, users, activeUser, setActiveUser, showIcons }) {
  const commonIconSize = '1rem'

  const handleDeleteUser = () => {
    const filteredUser = users.filter((userFromList) => userFromList.id !== user.id)
    setUsers(filteredUser)
  }

  return (
    <Fade in={showIcons}>
      <Flex gap="1.5rem">
        {activeUser ? (
          <Button onClick={() => setActiveUser(false)}>
            <GrFormClose cursor="pointer" size={commonIconSize} />
          </Button>
        ) : (
          <Button
            onClick={() => setActiveUser(true)}
            _hover={{
              background: 'green.100'
            }}
          >
            <BsCheck2All cursor="pointer" size={commonIconSize} />
          </Button>
        )}
        <Button
          onClick={handleDeleteUser}
          _hover={{
            background: 'red.500',
            color: 'gray.100'
          }}
        >
          <BiTrashAlt cursor="pointer" size={commonIconSize} />
        </Button>
      </Flex>
    </Fade>
  )
}
