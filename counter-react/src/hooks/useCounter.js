import { useState } from 'react'

export const useCounter = () => {
  const [count, setCount] = useState(0)

  const increment = () => (count >= 10 ? setCount(0) : setCount((prev) => prev + 1))

  const decrement = () => count > 0 && setCount((prev) => prev - 1)

  const reset = () => setCount(0)

  return {
    count,
    increment,
    decrement,
    reset,
  }
}
