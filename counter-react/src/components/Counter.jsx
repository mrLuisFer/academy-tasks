import { useCounter } from '../hooks/useCounter'
import CounterActions from './CounterActions'

export default function Counter() {
  const { decrement, increment, reset, count } = useCounter()

  return (
    <>
      <h1>{count}</h1>
      <CounterActions decrement={decrement} increment={increment} reset={reset} count={count} />
    </>
  )
}
