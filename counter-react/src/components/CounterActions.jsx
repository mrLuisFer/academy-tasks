export default function CounterActions({ decrement, increment, reset, count }) {
  const disableStyle = count === 0 ? 'disable' : ''

  return (
    <section className="btnList">
      <button type="button" className={`btn ${disableStyle}`} onClick={decrement}>
        Decrease
      </button>
      <button type="button" className="btn" onClick={reset}>
        Reset
      </button>
      <button type="button" className="btn" onClick={increment}>
        Increment
      </button>
    </section>
  )
}
